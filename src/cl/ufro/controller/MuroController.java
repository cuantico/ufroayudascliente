package cl.ufro.controller;

import java.util.List;

import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Muro;
import cl.ufro.service.MuroService;

public class MuroController {
	MuroService muroService = new MuroService();
	
	
	public Muro actualizar(Muro muro) {
		return muroService.actualizar(muro);
	}
	public List<Muro> ListarMuro(Ayudantia ayudantia){
	return muroService.ListarMuro(ayudantia);
	}
	/*
	public void registrar (Muro muro){
		muroService.registrar(muro);
	}
	public Muro identificar(Muro muro){
		return muroService.identificar(muro);
	}
	public Muro findById(Muro muro) {
		return muroService.findById(muro);
	}*/

}
