package cl.ufro.controller;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import com.sun.org.apache.bcel.internal.generic.IDIV;

import cl.ufro.bd.Usuario;
import cl.ufro.service.UsuarioService;
import cl.ufro.solicitud.ActualizarUsuario;
import cl.ufro.solicitud.IdentificarUsuario;
import cl.ufro.solicitud.RegistrarUsuario;

public class UsuarioController {

private UsuarioService usuarioService = new UsuarioService();

	public Usuario identificar(Usuario usuario){
		return usuarioService.identificar(usuario);
		
	}
	public void actualizar(Usuario usuario){
		 usuarioService.actualizar(usuario);
		
	}
	
	public void registrar(Usuario usuario){
		usuarioService.registrar(usuario);
	}

	public void guardar(Usuario usuario){
		usuarioService.guardar(usuario);
	}
	public Usuario findFirst(){
		return usuarioService.findFirst();
	}
	public Usuario getUsuarioSession(){
		return usuarioService.getUsuarioSession();
	}

}
