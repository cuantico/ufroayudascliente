package cl.ufro.controller;

import java.io.ObjectOutputStream;
import java.net.Socket;

import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Usuario;
import cl.ufro.service.AyudantiaService;
import cl.ufro.solicitud.RegistrarAyudantia;
import cl.ufro.solicitud.RegistrarUsuario;

public class AyudantiaController {

	private AyudantiaService ayudantiaService = new AyudantiaService();
	
	public Ayudantia registrar(Ayudantia ayudantia){
		return ayudantiaService.registrar(ayudantia);
	}
	public void actualizar(Ayudantia ayudantia) {
		ayudantiaService.actualizar(ayudantia);
	}
	public Ayudantia findById(Ayudantia ayudantia){
		return ayudantiaService.findById(ayudantia);
	}
	public Ayudantia identificar(Ayudantia ayudantia){
	 return ayudantiaService.identificar(ayudantia);
	}
}
