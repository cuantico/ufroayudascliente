package cl.ufro.controller;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;

import utils.ManagerSession;

import com.sun.org.apache.bcel.internal.generic.IDIV;

import cl.ufro.bd.Asignatura;
import cl.ufro.bd.Usuario;
import cl.ufro.service.AsignaturaService;
import cl.ufro.service.UsuarioService;
import cl.ufro.solicitud.IdentificarUsuario;
import cl.ufro.solicitud.RegistrarUsuario;

public class AsignaturaController {

	private AsignaturaService asignaturaService = new AsignaturaService();
	
	public List<Asignatura> listar(){
		return asignaturaService.listar();
	}

}
