package cl.ufro.controller;

import java.util.List;

import cl.ufro.bd.Interaccion;
import cl.ufro.service.InteraccionService;

public class InteraccionController {
	private InteraccionService interaccionService = new InteraccionService();

	public List<Interaccion> listar(){
		return interaccionService.listar();
	}

	public void guardar(Interaccion interaccion){
		interaccionService.guardar(interaccion);
	}
	public int funcion1(){
		return interaccionService.funcion1();
	}
	 
	
} 
