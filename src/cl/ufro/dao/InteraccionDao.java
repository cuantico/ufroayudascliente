package cl.ufro.dao;

import java.util.ArrayList;
import java.util.List;

import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Interaccion;
import utils.ObjetoBd;
import utils.ObjetoDao;

public class InteraccionDao extends ObjetoDao{

	public Interaccion find (Interaccion registro){
		return (Interaccion)super.find(registro);
	}
	public Interaccion findByIdAyudantia(Interaccion interaccion){
		List<ObjetoBd> lista = listAll();
		for(int i=0;i<lista.size();i++){
			Interaccion aux = (Interaccion)lista.get(i);
			if( aux.getIdAyudantia()== interaccion.getIdAyudantia())
				return aux;
		}
		return null;
	}
	public List<Interaccion> list(){ 
		List<ObjetoBd> lista = listAll();
		List<Interaccion> nuevo = new ArrayList<Interaccion>();
		for(int i=0;i<lista.size();i++){
			Interaccion aux = (Interaccion)lista.get(i);
			nuevo.add(aux);
		}
		return nuevo;		
	}

}
