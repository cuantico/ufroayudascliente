package cl.ufro.bd;

import java.io.Serializable;

import utils.ObjetoBd;

public class Notificacion extends ObjetoBd implements Serializable {

	private int r;

	public void setR(int r) {
		this.r = r;
	}

	public int getR() {
		return r;
	}
	
}
