package cl.ufro.bd;

import java.io.Serializable;

import utils.ObjetoBd;

public class Interaccion extends ObjetoBd implements Serializable {
private String correo;
private int idAyudantia;
private int idMyMsj;//Id del mensaje que escribo, se compara con el de la ayudantia que responde el server
public Interaccion(){
	addToPrimaryKey("idAyudantia");
}
public void setCorreo(String correo) {
	this.correo = correo;
}
public String getCorreo() {
	return correo;
}
public void setIdAyudantia(int idAyudantia) {
	this.idAyudantia = idAyudantia;
}
public int getIdAyudantia() {
	return idAyudantia;
}
public void setIdMyMsj(int idMyMsj) {
	this.idMyMsj = idMyMsj;
}
public int getIdMyMsj() {
	return idMyMsj;
}

}
