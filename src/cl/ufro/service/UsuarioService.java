package cl.ufro.service;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import utils.Config;
import utils.ManagerSession;

import cl.ufro.bd.Usuario;
import cl.ufro.dao.UsuarioDao;
import cl.ufro.solicitud.ActualizarUsuario;
import cl.ufro.solicitud.IdentificarUsuario;
import cl.ufro.solicitud.RegistrarUsuario;
/* Clase servicio contiene la logica del programa*/
public class UsuarioService {
	/*Instanciamos un Dao */
	private UsuarioDao usuarioDao = new UsuarioDao();

	/*Funcion registrar:
	 *Recibe por parametros un Usuario el cual es registrado con sus correo mediante una solicitud al servidor.
	 * 
	 */
	public void registrar(Usuario usuario){

		try {
			Socket socket = new Socket(Config.ipServidor,Config.puertoServidor);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			RegistrarUsuario registrarUsuario = new RegistrarUsuario();
			registrarUsuario.setCorreo(usuario.getCorreo());
			oos.writeObject(registrarUsuario);
		}catch (Exception e) {e.printStackTrace();}
	}
	/*Funcion identificar:
	 * Se instancia IdentificarUsuario el servidor acepta el objeto que contiene un correo
	 * y un pin, se comprueba la coincidencia de los campos y se retorna un usuario.
	 * En caso de que no coincida se retorna nulo.
	 * */
	public Usuario identificar(Usuario usuario){
		try {
			Socket socket = new Socket(Config.ipServidor,Config.puertoServidor);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			IdentificarUsuario identificarUsuario = new IdentificarUsuario();
			identificarUsuario.setCorreo(usuario.getCorreo());
			identificarUsuario.setPin(usuario.getPin());
			oos.writeObject(identificarUsuario);
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			usuario = (Usuario)ois.readObject();
			return usuario;
		}catch (Exception e) {e.printStackTrace();}
		return null;
		
	}
	/*Funcion actualizar:
	 * Recibe un Objeto Usuario.
	 * Una vez creada la conexion, se instacia Actualizar Usuario y se rellenan los campos actuales que contiene el usuario
	 * se envian al servidor para que sean actualizados.
	 * 
	 * */

	public void actualizar(Usuario usuario) {
		try {
			Socket socket = new Socket(Config.ipServidor,Config.puertoServidor);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			ActualizarUsuario actualizarUsuario = new ActualizarUsuario();
			actualizarUsuario.setCorreo(usuario.getCorreo());
			actualizarUsuario.setPin(usuario.getPin());
			actualizarUsuario.setNombre(usuario.getNombre());
			oos.writeObject(actualizarUsuario);
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			usuario = (Usuario)ois.readObject();
			//return usuario;
		}catch (Exception e) {e.printStackTrace();}
		//return null;
	}
	/*Funcion findByCorreo:
	 * Busca en el archivo local por correo.
	 * 
	 * */
	public Usuario findByCorreo(Usuario usuario){
		return usuarioDao.findByCorreo(usuario);
	}
	
	/*Funcion guardar:
	 * Recibe un usuario y lo a�ade al archivo local del cliente.
	 * 
	 * */

	public void guardar(Usuario usuario){
		usuarioDao.add(usuario);
	}
	/*Funcion findFirst:
	 * Busca en el archivo local el primer usuario que encuentre y lo retorna.
	 * 
	 * */
	public Usuario findFirst(){
		return usuarioDao.findFirst();
	}
	public Usuario getUsuarioSession(){
		return (Usuario)ManagerSession.findObject("usuario");
	} 

}
