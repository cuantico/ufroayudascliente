package cl.ufro.service;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import utils.Config;

import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Usuario;
import cl.ufro.dao.AyudantiaDao;
import cl.ufro.solicitud.ActualizarAyudantia;
import cl.ufro.solicitud.IdentificarAyudantia;
import cl.ufro.solicitud.IdentificarUsuario;
import cl.ufro.solicitud.RegistrarAyudantia;

public class AyudantiaService {
	private AyudantiaDao ayudantiaDao = new AyudantiaDao();
	/* Funcion registrar:
	 * Recibe una ayudantia y la envia por socket.
	 * 
	 * */
	public  Ayudantia registrar(Ayudantia ayudantia){
		try {
			Socket socket = new Socket(Config.ipServidor,Config.puertoServidor);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			RegistrarAyudantia registrarAyudantia = new RegistrarAyudantia();
			registrarAyudantia.setCorreoUsuario(ayudantia.getCorreoUsuario());
			registrarAyudantia.setMateria(ayudantia.getMateria());
			registrarAyudantia.setnPersonas(ayudantia.getnPersonas());
			registrarAyudantia.setOferta(ayudantia.getOferta());
			registrarAyudantia.setContenidos(ayudantia.getContenidos());
			registrarAyudantia.setFecha(ayudantia.getFecha());
			oos.writeObject(registrarAyudantia);
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			ayudantia = (Ayudantia)ois.readObject();
			return ayudantia;
		}catch (Exception e) {e.printStackTrace();}
		return null;
	}
	
	public Ayudantia identificar(Ayudantia ayudantia){
		try {
			Socket socket = new Socket(Config.ipServidor,Config.puertoServidor);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			IdentificarAyudantia identificarAyudantia = new IdentificarAyudantia();
			identificarAyudantia.setId(ayudantia.getId());
			identificarAyudantia.setFecha(ayudantia.getFecha());
			oos.writeObject(identificarAyudantia);
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			ayudantia = (Ayudantia)ois.readObject();
			return ayudantia;
		}catch (Exception e) {e.printStackTrace();}
		return null;	
	}

	public void actualizar(Ayudantia ayudantia) {
		try {
			Socket socket = new Socket(Config.ipServidor,Config.puertoServidor);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			ActualizarAyudantia actualizarAyudantia = new ActualizarAyudantia();
			actualizarAyudantia.setAyudante(ayudantia.getAyudante());
			actualizarAyudantia.setConfirmar(ayudantia.isConfirmar());
			actualizarAyudantia.setContenidos(ayudantia.getContenidos());
			actualizarAyudantia.setCorreoUsuario(ayudantia.getCorreoUsuario());
			actualizarAyudantia.setFecha(ayudantia.getFecha());
			actualizarAyudantia.setId(ayudantia.getId());
			actualizarAyudantia.setMateria(ayudantia.getMateria());
			actualizarAyudantia.setnPersonas(ayudantia.getnPersonas());
			actualizarAyudantia.setOferta(ayudantia.getOferta());
			actualizarAyudantia.setIdLastMsj(ayudantia.getIdLastMsj());
			oos.writeObject(actualizarAyudantia);
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			ayudantia = (Ayudantia)ois.readObject();
		}catch (Exception e) {e.printStackTrace();}
	}
	public Ayudantia findById(Ayudantia ayudantia){
		return ayudantiaDao.findById(ayudantia);
	}

	public void guardar(Ayudantia ayudantia){
		ayudantiaDao.add(ayudantia);
	}
	public Ayudantia findFirst(){
		return ayudantiaDao.findFirst();
	}

}
