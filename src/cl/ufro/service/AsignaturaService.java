package cl.ufro.service;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import utils.Config;
import utils.ManagerSession;

import cl.ufro.bd.Asignatura;
import cl.ufro.dao.UsuarioDao;
import cl.ufro.solicitud.ActualizarUsuario;
import cl.ufro.solicitud.IdentificarUsuario;
import cl.ufro.solicitud.ListarAsignatura;
import cl.ufro.solicitud.RegistrarUsuario;

public class AsignaturaService {

	public List<Asignatura> listar(){
		try {
			Socket socket = new Socket(Config.ipServidor,Config.puertoServidor);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject(new ListarAsignatura());
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			List<Asignatura> lista = (List<Asignatura>)ois.readObject();

			return lista;
		}catch (Exception e) {e.printStackTrace();}
		return new ArrayList<Asignatura>();
	}	
}
