package cl.ufro.service;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

import utils.Config;

import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Muro;
import cl.ufro.dao.MuroDao;
import cl.ufro.solicitud.ActualizarMuro;
import cl.ufro.solicitud.IdentificarMuro;
import cl.ufro.solicitud.ListarMuro;



public class MuroService {
	private MuroDao muroDao = new MuroDao();
	/* Funcion registrar:
	 * 
	 * 
	 * */
	/*
	public  void registrar(Muro muro){
		try {
			Socket socket = new Socket("127.0.0.1",5000);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			RegistrarMuro registrarMuro = new RegistrarMuro();
			oos.writeObject(registrarMuro);
		}catch (Exception e) {e.printStackTrace();}
	}*/
	/*
	public Muro identificar(Muro muro){
		try {
			Socket socket = new Socket("127.0.0.1",5000);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			IdentificarMuro identificarMuro = new IdentificarMuro();
			identificarMuro.setId(muro.getId());
			identificarMuro.setMensaje(muro.getMensaje());
			oos.writeObject(identificarMuro);
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			muro = (Muro)ois.readObject();
			return muro;
		}catch (Exception e) {e.printStackTrace();}
		return null;	
	}*/

	public Muro actualizar(Muro muro) {
		try {
			Socket socket = new Socket(Config.ipServidor,Config.puertoServidor);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			ActualizarMuro actualizarMuro = new ActualizarMuro();
			actualizarMuro.setCorreo(muro.getCorreo());
			actualizarMuro.setIdAyudantia(muro.getIdAyudantia());
			actualizarMuro.setMensaje(muro.getMensaje());

			oos.writeObject(actualizarMuro);
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			muro = (Muro)ois.readObject();
			return muro;
			}catch (Exception e) {e.printStackTrace();}
	return null;
	}
	public List<Muro> ListarMuro(Ayudantia ayudantia){
		try {
			Socket socket = new Socket(Config.ipServidor,Config.puertoServidor);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			ListarMuro listarMuro = new ListarMuro();
			listarMuro.setIdAyudantia(ayudantia.getId());
			oos.writeObject(listarMuro);
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			List<Muro> lista =(List<Muro>)ois.readObject();
			return lista;
		
	}catch (Exception e) {e.printStackTrace();
		}return null;
	}

	/*
	public Muro findById(Muro muro){
		return muroDao.findById(muro);
	}

	public void guardar(Muro muro){
		try {
			Socket socket = new Socket("127.0.0.1",5000);
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			ActualizarMuro actualizarMuro = new ActualizarMuro();
			actualizarMuro.setCorreo(muro.getCorreo());
			actualizarMuro.setIdMsj(muro.getIdMsj());
			actualizarMuro.setMensaje(muro.getMensaje());
			actualizarMuro.setId(muro.getId());
			oos.writeObject(actualizarMuro);
		
			}catch (Exception e) {e.printStackTrace();}
	}*/

	
	
}
