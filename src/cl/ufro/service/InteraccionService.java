package cl.ufro.service;



import java.util.ArrayList;
import java.util.List;

import utils.ObjetoBd;

import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Interaccion;
import cl.ufro.controller.AyudantiaController;

import cl.ufro.dao.InteraccionDao;

public class InteraccionService {
	private InteraccionDao interaccionDao = new InteraccionDao();

	public  List<Interaccion> listar(){
		return interaccionDao.list();
	}
	public int funcion1(){
		List<Interaccion> lista = listar();
		Interaccion interaccion;
		AyudantiaController ayudantiaController = new AyudantiaController();
		Ayudantia ayudantia = new Ayudantia();
		for(int i = 0;i<lista.size();i++){
			
			interaccion = lista.get(i);
			
			ayudantia.setId(interaccion.getIdAyudantia());
			Ayudantia aux=ayudantiaController.identificar(ayudantia);
			System.out.println("server: "+aux.getIdLastMsj());
			System.out.println("interaccion:"+interaccion.getIdMyMsj());
			if(aux.getIdLastMsj()>interaccion.getIdMyMsj())
				return 1;
			
		}
		return 0;
	}
	public Interaccion identificar(Interaccion interaccion){
		Interaccion interaccionBD = interaccionDao.findByIdAyudantia(interaccion); //BUSCAMOS POR ID
		if(interaccionBD!=null && interaccionBD.getIdAyudantia()==interaccion.getIdAyudantia())
			return interaccionBD;
		return null;
	}
	public void guardar(Interaccion interaccion){
		Interaccion aux = identificar(interaccion);
		if(aux==null)
			interaccionDao.add(interaccion);
		else{
			interaccionDao.update(interaccion);
		}
	}
}
