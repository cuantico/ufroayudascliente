package gui;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import utils.ManagerSession;

import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Interaccion;
import cl.ufro.bd.Muro;
import cl.ufro.bd.Usuario;
import cl.ufro.controller.AyudantiaController;
import cl.ufro.controller.InteraccionController;
import cl.ufro.controller.MuroController;
import cl.ufro.controller.UsuarioController;
import cl.ufro.solicitud.IdentificarAyudantia;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import java.util.List;

public class Principal extends JFrame {

	private JPanel contentPane;

	public static final int posX = 450;
	public static final int posY = 300;

	private LoginPanel loginPanel;
	private ListadoPanel listadoPanel;
	private HomePanel homePanel;
	private BienvenidaPanel BienvenidaPanel;
	private PinPanel pinPanel;
	private SolicitudPanel solicitudPanel;
	private MuroPanel muroPanel;
	private LoadPanel loadPanel;

	public static void main(String[] args) {
		Principal frame = new Principal();
		frame.setVisible(true);

		int i = 1; // Asignamos un contador para ver las ayudantias que existen
		while(true){ // Creamos un while para poder activar notificaciones
			try {
				Thread.sleep(5000); // Se asigna el tiempo para la actualizacion
				AyudantiaController ayudantiaController = new AyudantiaController();
				Ayudantia ayudantia = new Ayudantia();

				MuroController muroController = new MuroController();
				Muro muro = new Muro();

				InteraccionController interaccionController = new InteraccionController();


				JFrame frame2 = new JFrame();
				if(interaccionController.funcion1()==1){

					
					frame2.add(new Button("Nuevo Mensaje"));
					frame2.setLocation(frame.getLocation());
					frame2.setSize(120, 120);
					frame2.setVisible(true);
				}
				frame2.setVisible(false);
			} catch (InterruptedException e) {
				e.printStackTrace();}
		}
	}
	public Principal() {
		System.out.println("::Cliente::");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 305, 386);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.textHighlight);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		loginPanel = new LoginPanel(this);
		loginPanel.setBounds(0, 0, 289, 347);
		contentPane.add(loginPanel);

		homePanel = new HomePanel(this);
		homePanel.setBounds(0, 0, 289, 347);
		contentPane.add(homePanel);

		BienvenidaPanel = new BienvenidaPanel(this);
		BienvenidaPanel.setBounds(0, 0, 290, 347);
		contentPane.add(BienvenidaPanel);

		pinPanel = new PinPanel(this);
		pinPanel.setBounds(0, 0, 290, 347);
		contentPane.add(pinPanel);

		solicitudPanel = new SolicitudPanel(this);
		solicitudPanel.setBounds(0, 0, 289, 347);
		contentPane.add(solicitudPanel);

		listadoPanel = new ListadoPanel(this);
		listadoPanel.setBounds(0,0,289,347);
		contentPane.add(listadoPanel);

		muroPanel = new MuroPanel(this);
		muroPanel.setBounds(0,0,289,347);
		contentPane.add(muroPanel);

		loadPanel = new LoadPanel(this);
		loadPanel.setBounds(0,0,289,347);
		contentPane.add(loadPanel);



		// Se busca el primer usuario local para autologear a este.
		UsuarioController usuarioController = new UsuarioController();
		Usuario usuario = usuarioController.findFirst();

		if(usuario != null)
			usuario = usuarioController.identificar(usuario);
		if(usuario == null)
			verLoginPanel();
		else{
			//si el usuario se identifica correctamente accede al Home.
			ManagerSession.saveObject("usuario", usuario);
			verHomePanel();
		}

	}

	public void verHomePanel() {
		loginPanel.setVisible(false);		
		BienvenidaPanel.setVisible(false);
		pinPanel.setVisible(false);
		solicitudPanel.setVisible(false);
		listadoPanel.setVisible(false);
		muroPanel.setVisible(false);
		homePanel.init(); 
		homePanel.setVisible(true);
	}

	public void verLoginPanel() {
		homePanel.setVisible(false);
		BienvenidaPanel.setVisible(false);
		pinPanel.setVisible(false);
		solicitudPanel.setVisible(false);
		listadoPanel.setVisible(false);
		muroPanel.setVisible(false);
		loginPanel.init();
		loginPanel.setVisible(true);
	}
	public void verBienvenidaPanel() {
		loginPanel.setVisible(false);
		homePanel.setVisible(false);
		pinPanel.setVisible(false);
		solicitudPanel.setVisible(false);
		listadoPanel.setVisible(false);
		muroPanel.setVisible(false); 

		BienvenidaPanel.init();
		BienvenidaPanel.setVisible(true);
	}
	public void verPinPanel() {
		loginPanel.setVisible(false);
		homePanel.setVisible(false);
		BienvenidaPanel.setVisible(false);
		solicitudPanel.setVisible(false);
		listadoPanel.setVisible(false);
		muroPanel.setVisible(false);
		pinPanel.init();
		pinPanel.setVisible(true);
	}
	public void verSolicitudPanel(){
		loginPanel.setVisible(false);
		homePanel.setVisible(false);
		BienvenidaPanel.setVisible(false);
		pinPanel.setVisible(false);
		listadoPanel.setVisible(false);
		muroPanel.setVisible(false);
		solicitudPanel.init();
		solicitudPanel.setVisible(true);

	}
	public void verListadoPanel(){
		loginPanel.setVisible(false);
		homePanel.setVisible(false);
		BienvenidaPanel.setVisible(false);
		pinPanel.setVisible(false);
		solicitudPanel.setVisible(false);
		muroPanel.setVisible(false);
		listadoPanel.init();
		listadoPanel.setVisible(true);

	}
	public void verMuroPanel(){
		loginPanel.setVisible(false);
		homePanel.setVisible(false);
		BienvenidaPanel.setVisible(false);
		pinPanel.setVisible(false);
		solicitudPanel.setVisible(false);
		listadoPanel.setVisible(false);
		muroPanel.init();
		muroPanel.setVisible(true);

	}
	public void verLoadPanel(){
		loginPanel.setVisible(false);
		homePanel.setVisible(false);
		BienvenidaPanel.setVisible(false);
		pinPanel.setVisible(false);
		solicitudPanel.setVisible(false);
		listadoPanel.setVisible(false);
		muroPanel.setVisible(false);
		listadoPanel.init();
		listadoPanel.setVisible(true);

	}



}
