package gui;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;

import utils.ManagerSession;
import cl.ufro.bd.Asignatura;
import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Usuario;
import cl.ufro.controller.AsignaturaController;
import cl.ufro.controller.AyudantiaController;
import cl.ufro.controller.UsuarioController;
import cl.ufro.service.AyudantiaService;

import java.awt.Dimension;
import java.awt.SystemColor;
import java.awt.Color;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JComboBox;

public class ListadoPanel extends JPanel {

	private Principal principal;
	private JPanel pnlLista;
	private JComboBox hpBox;
	private JButton hpbtnBuscar;
	private JButton btnAtras;


	public ListadoPanel(Principal principal) {
		this();
		this.principal = principal;
	}

	public ListadoPanel() {
		setBackground(SystemColor.activeCaption);
		setBounds(0, 0, 299, 336);
		setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 80, 264, 209);
		add(scrollPane);

		pnlLista = new JPanel();
		scrollPane.setViewportView(pnlLista);
		pnlLista.setLayout(null);

		hpbtnBuscar = new JButton("Buscar");
		hpbtnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//int i = hpBox.getSelectedIndex();
				String codigo = (String)hpBox.getSelectedItem();
				ManagerSession.saveObject("codigo", codigo);
				//AyudantiaController ayudantiaController = new AyudantiaController();
				//Ayudantia ayudantia = new Ayudantia();
				//ayudantia.setId(1);
				//ayudantiaController.identificar(ayudantia);
				principal.verLoadPanel();
				principal.verListadoPanel();
				//List<Asignatura> lista = (List<Asignatura>)ManagerSession.findObject("asignaturas");
				//System.out.println(lista.get(i).getNombre());
				
			}
		});
		hpbtnBuscar.setBounds(10, 39, 89, 23);
		add(hpbtnBuscar);

		hpBox = new JComboBox();
		hpBox.setBounds(119, 38, 132, 31);
		add(hpBox);

		btnAtras = new JButton("Atras");
		btnAtras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				principal.verHomePanel();
			}
		});
		btnAtras.setBounds(93, 302, 73, 23);
		add(btnAtras);

	}

	public void init(){
		pnlLista.removeAll();
		
		String codigo = (String) ManagerSession.findObject("codigo");
	
		AyudantiaController ayudantiaController = new AyudantiaController();
		AsignaturaController asignaturaController = new AsignaturaController();
		List<Asignatura> lista = asignaturaController.listar();
		DefaultComboBoxModel boxModel = new DefaultComboBoxModel();
		for(int i= 0;i<lista.size();i++){
			Asignatura asignatura = lista.get(i);
			boxModel.addElement(asignatura.getCodigo()+"-"+asignatura.getNombre());
		}
////////////
		hpBox.setModel(boxModel);

		int n=20;int j=1;
		ItemListaAyudantiaPanel[] array = new ItemListaAyudantiaPanel[n];
		for(int i=1;i<n;i++){
			Ayudantia ayudantia = new Ayudantia();
			ayudantia.setId(i);
			ayudantia = (Ayudantia)ayudantiaController.identificar(ayudantia);//recupera las ayudantias del servidor
			if(ayudantia == null){
				break;
			}		
			if(ayudantia.getMateria().equals(codigo)) {
				array[j-1] = new ItemListaAyudantiaPanel(principal,ayudantia);
				array[j-1].setLocation(0, 35*(j-1));
				pnlLista.add(array[j-1]);
				pnlLista.setPreferredSize(new Dimension(300, (j)*35));
				j++;
			}			
		}
	}
}
