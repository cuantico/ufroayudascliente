package gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import utils.ManagerSession;

import cl.ufro.bd.Usuario;
import cl.ufro.controller.UsuarioController;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.SystemColor;

public class LoginPanel extends JPanel {
	private JTextField txtCorreo;
	
	private Principal principal;

	/**
	 * Create the panel.
	 */
	public LoginPanel(Principal principal){
		this();
		this.principal = principal;
	}
	public LoginPanel() {
		setBackground(SystemColor.activeCaption);
		setBounds(0, 0, 300, 200);
		setLayout(null);
		
		JLabel lblCorreo = new JLabel("Correo:");
		lblCorreo.setFont(new Font("Arial", Font.PLAIN, 12));
		lblCorreo.setBounds(26, 83, 70, 22);
		add(lblCorreo);
		
		JLabel lblufromailcl = new JLabel("@ufromail.cl");
		lblufromailcl.setFont(new Font("Arial", Font.PLAIN, 12));
		lblufromailcl.setBounds(204, 83, 86, 22);
		add(lblufromailcl);
		
		txtCorreo = new JTextField();
		txtCorreo.setColumns(10);
		txtCorreo.setBounds(80, 80, 122, 25);
		add(txtCorreo);
		
		JButton btnSiguiente = new JButton("Siguiente");
		btnSiguiente.setBackground(new Color(153, 204, 0));
		btnSiguiente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Usuario usuario = new Usuario();
				usuario.setCorreo(txtCorreo.getText()+"@ufromail.cl");
				
				UsuarioController usuarioController = new UsuarioController();
				usuarioController.registrar(usuario);
				
				ManagerSession.saveObject("usuario", usuario);
				principal.verPinPanel();
			}
		});
		btnSiguiente.setBounds(88, 131, 117, 49);
		add(btnSiguiente);
		
		JLabel lblUfroayudas = new JLabel("Bienvenido a UFROayudas");
		lblUfroayudas.setToolTipText("");
		lblUfroayudas.setFont(new Font("Malgun Gothic", Font.PLAIN, 17));
		lblUfroayudas.setBounds(47, 11, 224, 25);
		add(lblUfroayudas);
		
		JLabel lblIngresaTuCorreo = new JLabel("Ingresa tu correo para empezar");
		lblIngresaTuCorreo.setForeground(new Color(0, 0, 0));
		lblIngresaTuCorreo.setFont(new Font("Malgun Gothic Semilight", Font.BOLD, 10));
		lblIngresaTuCorreo.setBounds(57, 47, 214, 14);
		add(lblIngresaTuCorreo);

	}
	public void init() {
		txtCorreo.setText("");
		
		//txtClave.setText("");
		
	}
}
