package gui;

import javax.swing.JPanel;
import java.util.Calendar;
import java.util.List;

import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import utils.ManagerSession;

import cl.ufro.bd.Asignatura;
import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Usuario;
import cl.ufro.controller.AsignaturaController;
import cl.ufro.controller.AyudantiaController;
import cl.ufro.controller.UsuarioController;
import cl.ufro.service.AyudantiaService;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextPane;
import java.util.Calendar;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.SystemColor;
public class SolicitudPanel extends JPanel {

	private Principal principal;
	private JTextField txtMonto;
	private JTextField txtPersonas;
	private JTextField txtFecha;
	private JTextField txtContenidos;
	private JComboBox materiaBox;

	public SolicitudPanel(Principal principal){
		this();
		this.principal = principal;
	}
	public SolicitudPanel() {
		setBackground(SystemColor.activeCaption);
		setBounds(0,0,262,376);
		setLayout(null);

		JButton btnSolicitarAyuda2 = new JButton("Solicitar");
		btnSolicitarAyuda2.setBackground(new Color(153, 204, 0));
		btnSolicitarAyuda2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Usuario usuario =(Usuario)ManagerSession.findObject("usuario");
				AyudantiaController ayudantiaController = new AyudantiaController();

				Ayudantia ayudantia = new Ayudantia();

				// El pin lo asigna el servidor
				ayudantia.setCorreoUsuario(usuario.getCorreo());
				ayudantia.setMateria((String)materiaBox.getSelectedItem());
				ayudantia.setnPersonas(txtPersonas.getText());
				ayudantia.setOferta(txtMonto.getText());
				ayudantia.setContenidos(txtContenidos.getText());
				ayudantia.setFecha(txtFecha.getText());
				ayudantia= ayudantiaController.registrar(ayudantia);
				ManagerSession.saveObject("ayudantia", ayudantia);
				//Llamamos al muro asignado con la ayudantia pedida
				principal.verMuroPanel();

			}
		});

		btnSolicitarAyuda2.setBounds(82, 208, 87, 20);
		add(btnSolicitarAyuda2);

		JLabel lblMateria = new JLabel("Materia:");
		lblMateria.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblMateria.setBounds(21, 34, 102, 14);
		add(lblMateria);

		JLabel lblFecha = new JLabel("Fecha ayudantia");
		lblFecha.setVerifyInputWhenFocusTarget(false);
		lblFecha.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblFecha.setBounds(21, 88, 94, 14);
		add(lblFecha);



		JLabel lblFechaActual = new JLabel("Fecha actual:");
		lblFechaActual.setBounds(21, 63, 102, 14);
		add(lblFechaActual);

		Calendar c= Calendar.getInstance();

		JLabel lblNewLabel = new JLabel(""+Integer.toString(c.get(Calendar.DATE)));
		lblNewLabel.setBounds(133, 63, 20, 14);
		add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("/ "+Integer.toString(c.get(Calendar.MONTH)));
		lblNewLabel_1.setBounds(154, 63, 20, 14);
		add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("/ "+Integer.toString(c.get(Calendar.YEAR)));
		lblNewLabel_2.setBounds(174, 63, 47, 14);
		add(lblNewLabel_2);

		JLabel lblMontoclp = new JLabel("Monto [CLP]");
		lblMontoclp.setBounds(21, 120, 102, 14);
		add(lblMontoclp);

		txtMonto = new JTextField();
		txtMonto.setBounds(125, 117, 96, 20);
		add(txtMonto);
		txtMonto.setColumns(10);

		JLabel lblNpersonas = new JLabel("N\u00B0personas:");
		lblNpersonas.setBounds(21, 145, 86, 14);
		add(lblNpersonas);

		txtPersonas = new JTextField();
		txtPersonas.setBounds(125, 142, 96, 20);
		add(txtPersonas);
		txtPersonas.setColumns(10);

		txtFecha = new JTextField();
		txtFecha.setBounds(125, 86, 96, 20);
		add(txtFecha);
		txtFecha.setColumns(10);

		JLabel lblComentarios = new JLabel("Contenidos:");
		lblComentarios.setBounds(21, 170, 86, 14);
		add(lblComentarios);

		txtContenidos = new JTextField();
		txtContenidos.setBounds(125, 167, 96, 20);
		add(txtContenidos);
		txtContenidos.setColumns(10);

		materiaBox = new JComboBox();
		materiaBox.setBounds(125, 32, 96, 20);
		add(materiaBox);

		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				principal.verHomePanel();
			}
		});
		btnVolver.setBackground(new Color(153, 204, 0));
		btnVolver.setBounds(82, 239, 87, 20);
		add(btnVolver);

	}
	public void init(){
		AsignaturaController asignaturaController = new AsignaturaController();
		List <Asignatura> lista = asignaturaController.listar();
		DefaultComboBoxModel boxModel = new DefaultComboBoxModel();
		for(int i=0; i<lista.size();i++){
			Asignatura asignatura = lista.get(i);
			boxModel.addElement(asignatura.getCodigo()+"-"+asignatura.getNombre());
		}
		materiaBox.setModel(boxModel);
	}
}
