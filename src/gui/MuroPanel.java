package gui;

import javax.swing.JPanel;
import java.util.Calendar;
import java.util.List;

import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import utils.ManagerSession;

import cl.ufro.bd.Ayudantia;
import cl.ufro.bd.Interaccion;
import cl.ufro.bd.Muro;
import cl.ufro.bd.Usuario;
import cl.ufro.controller.AyudantiaController;
import cl.ufro.controller.InteraccionController;
import cl.ufro.controller.MuroController;
import cl.ufro.controller.UsuarioController;
import cl.ufro.service.AyudantiaService;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextPane;
import java.util.Calendar;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.SystemColor;
import java.awt.TextArea;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
public class MuroPanel extends JPanel {

	private Principal principal;
	private JTextField txtMonto;
	private JTextField txtMateria;
	private JTextField txtPersonas;
	private JTextField txtFecha;
	private JTextField txtContenidos;
	private JTextField txtMensaje;
	private JButton btnVolver;
	private JButton btnEnviar;
	private JButton btnAceptar;
	private JTextArea textArea;
	private JLabel lblAyudante;;


	private Usuario usu ;
	public MuroPanel(Principal principal){
		this();
		this.principal = principal;
	}
	public MuroPanel() {
		setBackground(SystemColor.activeCaption);
		setBounds(0,0,262,376);
		setLayout(null);

		JLabel lblMateria = new JLabel("Materia:");
		lblMateria.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblMateria.setBounds(21, 34, 102, 14);
		add(lblMateria);

		JLabel lblFecha = new JLabel("Fecha ayudantia");
		lblFecha.setVerifyInputWhenFocusTarget(false);
		lblFecha.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblFecha.setBounds(21, 59, 94, 14);
		add(lblFecha);



		JLabel lblFechaActual = new JLabel("Fecha actual:");
		lblFechaActual.setBounds(90, 11, 102, 14);
		add(lblFechaActual);

		Calendar c= Calendar.getInstance();

		JLabel lblNewLabel = new JLabel(""+Integer.toString(c.get(Calendar.DATE)));
		lblNewLabel.setBounds(172, 11, 20, 14);
		add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("/"+Integer.toString(c.get(Calendar.MONTH)));
		lblNewLabel_1.setBounds(185, 11, 20, 14);
		add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("/"+Integer.toString(c.get(Calendar.YEAR)));
		lblNewLabel_2.setBounds(202, 11, 60, 14);
		add(lblNewLabel_2);

		JLabel lblMontoclp = new JLabel("Monto[CLP]:");
		lblMontoclp.setBounds(128, 119, 64, 14);
		add(lblMontoclp);

		txtMonto = new JTextField();
		txtMonto.setEditable(false);
		txtMonto.setBounds(190, 116, 47, 20);
		add(txtMonto);
		txtMonto.setColumns(10);

		txtMateria = new JTextField();
		txtMateria.setEditable(false);
		txtMateria.setBounds(82, 32, 155, 20);
		add(txtMateria);
		txtMateria.setColumns(10);

		JLabel lblNpersonas = new JLabel("N\u00B0personas:");
		lblNpersonas.setBounds(21, 119, 69, 14);
		add(lblNpersonas);

		txtPersonas = new JTextField();
		txtPersonas.setEditable(false);
		txtPersonas.setBounds(90, 116, 28, 20);
		add(txtPersonas);
		txtPersonas.setColumns(10);

		txtFecha = new JTextField();
		txtFecha.setEditable(false);
		txtFecha.setBounds(126, 57, 111, 20);
		add(txtFecha);
		txtFecha.setColumns(10);

		JLabel lblComentarios = new JLabel("Contenidos:");
		lblComentarios.setBounds(21, 84, 86, 14);
		add(lblComentarios);

		txtContenidos = new JTextField();
		txtContenidos.setEditable(false);
		txtContenidos.setBounds(125, 84, 112, 24);
		add(txtContenidos);
		txtContenidos.setColumns(10);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(21, 204, 216, 86);
		add(scrollPane);

		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);

		txtMensaje = new JTextField();
		txtMensaje.setBounds(21, 171, 117, 20);
		add(txtMensaje);


		btnEnviar = new JButton("Enviar");
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Usuario usuario  = (Usuario)ManagerSession.findObject("usuario");
				Ayudantia ayudantiaMuro  = (Ayudantia)ManagerSession.findObject("ayudantia");
				
				String name = usuario.getNombre();
				String msj = name+":"+txtMensaje.getText();
				
				Muro muro = new Muro();
				
				muro.setMensaje(msj);
				muro.setCorreo(usuario.getCorreo());
				muro.setIdAyudantia(ayudantiaMuro.getId());
				
				MuroController muroController = new MuroController();
				muro= muroController.actualizar(muro);
				ayudantiaMuro.setIdLastMsj(muro.getIdMsj());
				
				AyudantiaController ayudantiaController = new AyudantiaController();
				ayudantiaController.actualizar(ayudantiaMuro);
				
				InteraccionController interaccionController = new InteraccionController();
				Interaccion interaccion = new Interaccion();
				interaccion.setIdMyMsj(muro.getIdMsj());
				interaccion.setIdAyudantia(ayudantiaMuro.getId());
				interaccionController.guardar(interaccion);
				
				principal.verLoadPanel();
				principal.verMuroPanel();

			}
		});
		btnEnviar.setBounds(148, 170, 89, 23);
		add(btnEnviar);


		txtMensaje.setColumns(10);

		btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				principal.verListadoPanel();
			}
		});
		btnVolver.setBounds(21, 316, 216, 23);


		add(btnVolver);
		lblAyudante = new JLabel("");
		lblAyudante.setBounds(21, 147, 127, 14);
		add(lblAyudante);

		btnAceptar = new JButton("Aceptar ");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Ayudantia ayudantiaMuro  = (Ayudantia)ManagerSession.findObject("ayudantia");
				Usuario usu  = (Usuario)ManagerSession.findObject("usuario");
				
				if(usu.getCorreo().equals(ayudantiaMuro.getCorreoUsuario())){
					
					int Y =JOptionPane.showConfirmDialog(null,"Ayudantia Con: "+ayudantiaMuro.getAyudante(),"Confirmacion", JOptionPane.YES_NO_OPTION);
					if(Y==1)
						ayudantiaMuro.setConfirmar(true);
					else
						ayudantiaMuro.setConfirmar(false);
				}else{
					ayudantiaMuro.setAyudante(usu.getCorreo());	
					lblAyudante.setText(""+ayudantiaMuro.getAyudante());
					
				}
				AyudantiaController ayudantiaController = new AyudantiaController();
			    ayudantiaController.actualizar(ayudantiaMuro);
				principal.verLoadPanel();
				principal.verMuroPanel();
				 
			}
		});
		btnAceptar.setBackground(Color.GREEN);
		btnAceptar.setBounds(155, 144, 82, 23);
		add(btnAceptar);


	}
	public void init(){
		/*Se recupera la ayudantia seleccionada desde la lista de ayudantias mediante
		 * el managerSesion y se obtienen los datos  de la seleccionada para 
		 * cargarlos en muroPanel.
		 */
		Ayudantia ayudantiaMuro  = (Ayudantia)ManagerSession.findObject("ayudantia");
		AyudantiaController ayudantiaController = new AyudantiaController();
		ayudantiaMuro = ayudantiaController.identificar(ayudantiaMuro);//se pide al server la misma que se selecciono en la vista anterior

		txtMateria.setText(""+ayudantiaMuro.getMateria());
		txtContenidos.setText(""+ayudantiaMuro.getContenidos());
		txtFecha.setText(""+ayudantiaMuro.getFecha());
		txtMonto.setText(""+ayudantiaMuro.getOferta());
		txtPersonas.setText(""+ayudantiaMuro.getnPersonas());

		
		if(ayudantiaMuro.getAyudante()!=null)
			lblAyudante.setText(""+ayudantiaMuro.getAyudante());
		else
			lblAyudante.setText("");
		
		Muro muro = new Muro();
		muro.setIdAyudantia(ayudantiaMuro.getId());
		MuroController muroController = new MuroController();
		List<Muro> lista = muroController.ListarMuro(ayudantiaMuro);
		String msj = "";
		for(int i= 0;i<lista.size();i++){
			Muro m = lista.get(i);
			msj = msj+m.getMensaje()+"\n";			
		}
		textArea.setText(msj);
	}
}
