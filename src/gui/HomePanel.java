package gui;

import javax.swing.JPanel;
import javax.swing.JLabel;

import cl.ufro.bd.Usuario;

import utils.ManagerSession;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.SystemColor;
import java.awt.Color;

public class HomePanel extends JPanel {


	private JLabel lblSaludo;
	private Principal principal;
	private JButton btnVerAyudantias;
	private JButton btnSolicitarAyudantia;
	
	public HomePanel(Principal principal) {
		this();
		this.principal = principal;
	}
	
	public HomePanel() {
		setBackground(SystemColor.activeCaption);
		setBounds(0, 0, 327, 346);
		setLayout(null);
		
		lblSaludo = new JLabel("Hola");
		lblSaludo.setBounds(10, 55, 242, 15);
		add(lblSaludo);
		
		btnSolicitarAyudantia = new JButton("Solicitar ayudantia");
		btnSolicitarAyudantia.setBackground(new Color(153, 204, 0));
		btnSolicitarAyudantia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				principal.verSolicitudPanel();
			}
		});
		btnSolicitarAyudantia.setBounds(91, 244, 139, 40);
		add(btnSolicitarAyudantia);
		
		JLabel lblBienvenidoAlHome = new JLabel("Bienvenido al Home");
		lblBienvenidoAlHome.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblBienvenidoAlHome.setBounds(10, 11, 242, 14);
		add(lblBienvenidoAlHome);
		
		btnVerAyudantias = new JButton("Ver Ayudantias");
		btnVerAyudantias.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				principal.verListadoPanel();
				
			}
		});
		btnVerAyudantias.setBackground(new Color(153, 204, 0));
		btnVerAyudantias.setBounds(91, 193, 139, 40);
		add(btnVerAyudantias);

	}
	
	public void init(){
		Usuario usuario =(Usuario)ManagerSession.findObject("usuario");
		lblSaludo.setText("Bienvenido " +usuario.getNombre());
	}
}
