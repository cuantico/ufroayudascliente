package gui;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.border.BevelBorder;
import javax.swing.JLabel;

import utils.ManagerSession;

import cl.ufro.bd.Ayudantia;

import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ItemListaAyudantiaPanel extends JPanel {
private Ayudantia ayudantia;
private Principal principal;

private JLabel iLAPlblfecha;

/**
 * @wbp.parser.constructor
 */
public ItemListaAyudantiaPanel(Principal principal,Ayudantia a) {
	this(a);
	this.principal = principal;	
}

	public ItemListaAyudantiaPanel(Ayudantia a) {
		this.ayudantia = a;
		setBackground(SystemColor.controlHighlight);
		setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		setLayout(null);
		setSize(260,35);
		JButton iLAPbtn = new JButton("Entrar");
		
		iLAPbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Ver ayudantia con Id: "+ayudantia.getId());
				ManagerSession.saveObject("ayudantia", ayudantia);
				principal.verMuroPanel();
			}
		});
		iLAPbtn.setForeground(new Color(255, 255, 255));
		iLAPbtn.setBackground(new Color(0, 102, 204));
		iLAPbtn.setBounds(161, 8, 89, 18);
		add(iLAPbtn);
		
		JLabel iLAPlblprecio = new JLabel("$"+ayudantia.getOferta());
		iLAPlblprecio.setForeground(new Color(0, 0, 0));
		iLAPlblprecio.setBounds(27, 10, 46, 14);
		add(iLAPlblprecio);
		JLabel iLAPlblfecha = new JLabel(ayudantia.getFecha());
		iLAPlblfecha.setForeground(new Color(0, 0, 0));
		iLAPlblfecha.setBounds(85, 10, 76, 14);	
		add(iLAPlblfecha);
	}
}
