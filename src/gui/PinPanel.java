package gui;

import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;

import utils.ManagerSession;
import cl.ufro.bd.Usuario;
import cl.ufro.controller.UsuarioController;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.Font;
import javax.swing.SwingConstants;

public class PinPanel extends JPanel {

	private Principal principal;
	private JTextField txtPin;

	public PinPanel(Principal principal) {
		this();
		this.principal = principal;
	}

	public PinPanel() {
		setBackground(SystemColor.activeCaption);
		setBounds(0, 0, 318, 260);
		setLayout(null);

		JLabel lblNombre = new JLabel("Pin:");
		lblNombre.setBounds(73, 99, 36, 15);
		add(lblNombre);

		txtPin = new JTextField();
		txtPin.setBounds(94, 96, 140, 19);
		add(txtPin);
		txtPin.setColumns(10);

		JButton btnRegistro = new JButton("Registro");
		btnRegistro.setBackground(new Color(153, 204, 0));
		btnRegistro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Usuario usuario =(Usuario)ManagerSession.findObject("usuario");
				Usuario user = new Usuario();

				user.setCorreo(usuario.getCorreo());
				user.setPin(txtPin.getText());
				ManagerSession.saveObject("usuario",user);

				UsuarioController usuarioController = new UsuarioController();
				user = usuarioController.identificar(user);

				if(user == null){
					ManagerSession.removeObject("usuario");
					principal.verLoginPanel();
				}
				else{
					ManagerSession.saveObject("usuario", user);
					usuarioController.guardar(user);
					principal.verBienvenidaPanel();
				}
			}
		});
		btnRegistro.setBounds(103, 165, 117, 70);
		add(btnRegistro);
		
		JLabel lblIngreseSuPin = new JLabel("Ingrese su pin");
		lblIngreseSuPin.setVerticalAlignment(SwingConstants.BOTTOM);
		lblIngreseSuPin.setFont(new Font("Malgun Gothic", Font.PLAIN, 13));
		lblIngreseSuPin.setBounds(109, 48, 99, 14);
		add(lblIngreseSuPin);

	}

	public void init(){
		txtPin.setText("");
	}
}
