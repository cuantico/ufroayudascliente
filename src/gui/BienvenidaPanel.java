package gui;

import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;

import utils.ManagerSession;
import cl.ufro.bd.Usuario;
import cl.ufro.controller.UsuarioController;
import java.awt.SystemColor;
import java.awt.Color;

public class BienvenidaPanel extends JPanel {

	private Principal principal;
	private JTextField txtNombre;

	public BienvenidaPanel(Principal principal) {
		this();
		this.principal = principal;
	}
	
	public BienvenidaPanel() {
		setBackground(SystemColor.activeCaption);
		setBounds(0, 0, 342, 300);
		setLayout(null);
		
		
		JLabel lblNombre = new JLabel("Bienvenido, Ingrese su nombre: ");
		lblNombre.setBounds(10, 41, 240, 15);
		add(lblNombre);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(109, 100, 114, 19);
		add(txtNombre);
		txtNombre.setColumns(10);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setBackground(new Color(153, 204, 0));
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Usuario usuario =(Usuario)ManagerSession.findObject("usuario");
				Usuario user = new Usuario();
			
				user.setNombre(txtNombre.getText());
				user.setCorreo(usuario.getCorreo());
				user.setPin(usuario.getPin());
				
				ManagerSession.saveObject("usuario",user);
				UsuarioController usuarioController = new UsuarioController();
				usuarioController.actualizar(user);
				System.out.println(""+user.getCorreo());	
				principal.verHomePanel();
			}
		});
		btnGuardar.setBounds(109, 173, 117, 25);
		add(btnGuardar);

	}
	
	public void init(){
		Usuario usuario = (Usuario)ManagerSession.findObject("usuario");
		txtNombre.setText(usuario.getNombre());
	}
}
